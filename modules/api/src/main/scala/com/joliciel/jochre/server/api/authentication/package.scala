package com.joliciel.jochre.server.api

package object authentication {
  case class RoleName(name: String)
}
