package com.joliciel.jochre.server.api

import enumeratum._

import java.time.{Instant, ZoneId, ZonedDateTime}
import java.util.UUID

package object model {
  sealed trait AnalysisStatusCode extends EnumEntry

  object AnalysisStatusCode extends Enum[AnalysisStatusCode] with DoobieEnum[AnalysisStatusCode] {
    val values = findValues

    case object Pending extends AnalysisStatusCode
    case object Underway extends AnalysisStatusCode
    case object Available extends AnalysisStatusCode
    case object Failed extends AnalysisStatusCode
    case object Retry extends AnalysisStatusCode
    case object Deleted extends AnalysisStatusCode
    case object Expired extends AnalysisStatusCode

    def toEnum(code: AnalysisStatusCode): String = code.entryName

    def fromEnum(s: String): Option[AnalysisStatusCode] = AnalysisStatusCode.withNameOption(s)
  }


  sealed trait AnalysisStatus {
    def code: AnalysisStatusCode
    def when: Instant
  }

  object AnalysisStatus {
    case class Pending(when: Instant = Instant.now()) extends AnalysisStatus {
      val code = AnalysisStatusCode.Pending
    }
    case class Underway(when: Instant = Instant.now()) extends AnalysisStatus {
      val code = AnalysisStatusCode.Underway
    }
    case class Available(when: Instant = Instant.now()) extends AnalysisStatus {
      val code = AnalysisStatusCode.Available
    }
    case class Failed(errorCode: String, errorDescription: String, when: Instant = Instant.now()) extends AnalysisStatus {
      val code = AnalysisStatusCode.Failed
    }
    case class Retry(errorCode: String, errorDescription: String, when: Instant = Instant.now()) extends AnalysisStatus {
      val code = AnalysisStatusCode.Retry
    }
    case class Deleted(when: Instant = Instant.now()) extends AnalysisStatus {
      val code = AnalysisStatusCode.Deleted
    }
    case class Expired(when: Instant = Instant.now()) extends AnalysisStatus {
      val code = AnalysisStatusCode.Expired
    }

    def fromStatusElements(code: AnalysisStatusCode, when: Instant, errorCode: Option[String], errorDescription: Option[String]): AnalysisStatus = {
      code match {
        case AnalysisStatusCode.Pending => AnalysisStatus.Pending(when)
        case AnalysisStatusCode.Underway => AnalysisStatus.Underway(when)
        case AnalysisStatusCode.Available => AnalysisStatus.Available(when)
        case AnalysisStatusCode.Deleted => AnalysisStatus.Deleted(when)
        case AnalysisStatusCode.Expired => AnalysisStatus.Expired(when)
        case AnalysisStatusCode.Failed if errorCode.isDefined && errorDescription.isDefined =>
          AnalysisStatus.Failed(errorCode.get, errorDescription.get, when)
        case AnalysisStatusCode.Retry if errorCode.isDefined && errorDescription.isDefined =>
          AnalysisStatus.Retry(errorCode.get, errorDescription.get, when)
        case other => throw new Exception(f"Impossible analysis status: $other, errorCode=${errorCode}, errorDescription=${errorDescription}")
      }
    }
  }

  case class ProjectCode(code: String) {
    override def toString: String = code
  }

  case class FileId(id: String) {
    override def toString: String = id
  }

  case class BatchId(id: UUID) {
    override def toString: String = id.toString
  }

  case class ImageReference(file: FileId, url: String)

  sealed trait DeliveryMode extends EnumEntry

  object DeliveryMode extends Enum[DeliveryMode] {
    val values = findValues

    case object Local extends DeliveryMode
    case object AmazonS3 extends DeliveryMode
  }

  case class BatchRequest(images: Seq[ImageReference], pathPrefix: Option[String] = None)

  object BatchRequestHelper {
    val example: BatchRequest = BatchRequest(
      images = Seq(
        ImageReference(FileId("nybc200058_0008.xml"), "https://iiif.archive.org/iiif/nybc200058$8/full/1800,/0/default.jpg"),
        ImageReference(FileId("nybc200058_0009.xml"), "https://iiif.archive.org/iiif/nybc200058$9/full/1800,/0/default.jpg")
      ),
      pathPrefix = Some("nybc/nybc200058/")
    )
  }

  case class BatchResponse(id: BatchId)

  object BatchResponseHelper {
    val example: BatchResponse = BatchResponse(
      id = BatchId(UUID.fromString("f4944b2c-bbd4-49c2-a0d7-dd188c564e0d"))
    )
  }

  case class Batch(
    id: BatchId,
    projectCode: ProjectCode,
    owner: String,
    created: Instant = Instant.now(),
    pathPrefix: String = "",
    completed: Option[Instant] = None
  )

  case class BatchStatistics(
    batchId: BatchId,
    owner: String,
    created: Instant,
    requests: Map[AnalysisStatusCode, Int],
  )

  object BatchStatisticsHelper {
    val createDate = ZonedDateTime.of(2023, 3, 29, 17, 35, 12, 0, ZoneId.of("UTC"))
    val example: BatchStatistics = BatchStatistics(
      batchId = BatchId(UUID.fromString("f4944b2c-bbd4-49c2-a0d7-dd188c564e0d")),
      owner = "jimi@hendrix.org",
      created = createDate.toInstant,
      requests = Map(
        AnalysisStatusCode.Pending -> 372,
        AnalysisStatusCode.Available -> 123,
        AnalysisStatusCode.Failed -> 8,
      )
    )
  }

  case class AnalysisRequestId(id: Long)
  case class AnalysisRequest(
    id: AnalysisRequestId,
    batchId: BatchId,
    fileId: FileId,
    url: String,
    status: AnalysisStatus
  )

  case class PaginatedAnalysisRequests(totalCount: Int, requests: Seq[AnalysisRequest])

  object PaginatedAnalysisRequestHelper {
    val createDate = ZonedDateTime.of(2023, 3, 29, 17, 35, 12, 0, ZoneId.of("UTC"))
    val example: PaginatedAnalysisRequests = PaginatedAnalysisRequests(
      totalCount = 42,
      requests = Seq(
        AnalysisRequest(AnalysisRequestId(1), BatchId(UUID.fromString("f4944b2c-bbd4-49c2-a0d7-dd188c564e0d")), FileId("nybc200058_0008.xml"), "https://iiif.archive.org/iiif/nybc200058$8/full/1800,/0/default.jpg", AnalysisStatus.Available(when = createDate.toInstant)),
        AnalysisRequest(AnalysisRequestId(2), BatchId(UUID.fromString("f4944b2c-bbd4-49c2-a0d7-dd188c564e0d")), FileId("nybc200058_0009.xml"), "https://iiif.archive.org/iiif/nybc200058$9/full/1800,/0/default.jpg", AnalysisStatus.Failed(when = createDate.plusMinutes(2).toInstant, errorCode = "fileNotFound", errorDescription = "404 file not found"))
      )
    )
  }
}
