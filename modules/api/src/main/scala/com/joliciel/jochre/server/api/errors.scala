package com.joliciel.jochre.server.api

class NotFoundException(message: String) extends Exception(message)
class BadRequestException(message: String) extends Exception(message)
