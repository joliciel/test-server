package com.joliciel.jochre.server.api.analysis

import com.joliciel.jochre.server.api.Types.Requirements
import com.joliciel.jochre.server.api.model._
import com.joliciel.jochre.server.api.{HttpError, HttpErrorMapper}
import zio._
import zio.stream.{ZPipeline, ZStream}

import java.time.ZonedDateTime
import java.util.UUID

trait AnalysisLogic extends HttpErrorMapper {
  def postBatchLogic(projectCode: ProjectCode, request: BatchRequest): ZIO[Requirements, HttpError, BatchResponse] =
    (for {
      batchId <- ZIO.attempt(BatchId(UUID.randomUUID()))
    } yield BatchResponse(batchId))
      .tapErrorCause(error => ZIO.logErrorCause(s"Unable to post batch", error))
      .mapError(mapToHttpError(_))

  def getBatchLogic(projectCode: ProjectCode, batchId: BatchId): ZIO[Requirements, HttpError, BatchStatistics] =
    (for {
      stats <- ZIO.attempt(BatchStatisticsHelper.example)
    } yield stats)
      .tapErrorCause(error => ZIO.logErrorCause(s"Unable to get batch", error))
      .mapError(mapToHttpError(_))

  def getBatchSummaryLogic(projectCode: ProjectCode, batchId: BatchId): ZIO[Requirements, HttpError, ZStream[Any, Throwable, Byte]] =
    (for {
      stats <- ZIO.attempt("Hello")
    } yield {
      ZStream(stats)
        .via(ZPipeline.utf8Encode)
    })
      .tapErrorCause(error => ZIO.logErrorCause(s"Unable to get batch", error))
      .mapError(mapToHttpError(_))

  def getFilesLogic(projectCode: ProjectCode, batchId: Option[BatchId], since: Option[ZonedDateTime], status: List[AnalysisStatusCode], start: Int, end: Int): ZIO[Requirements, HttpError, PaginatedAnalysisRequests] =
    (for {
      page <- ZIO.attempt(PaginatedAnalysisRequestHelper.example)
    } yield page)
      .tapErrorCause(error => ZIO.logErrorCause(s"Unable to get files", error))
      .mapError(mapToHttpError(_))

  def getAnalysisLogic(projectCode: ProjectCode, batchId: BatchId, analysisRequestId: AnalysisRequestId): ZIO[Requirements, HttpError, ZStream[Any, Throwable, Byte]] =
    (for {
      alto4Xml <- ZIO.attempt("<Hello>hello</Hello>")
    } yield {
      ZStream(alto4Xml)
        .via(ZPipeline.utf8Encode)
    })
      .tapErrorCause(error => ZIO.logErrorCause(s"Unable to get analysis", error))
      .mapError { mapToHttpError(_) }

  def deleteAnalysisLogic(projectId: ProjectCode, batchId: BatchId, fileId: FileId): ZIO[Requirements, HttpError, OkResponse] =
    ZIO.attempt(OkResponse())
      .tapErrorCause(error => ZIO.logErrorCause(s"Unable to delete analysis", error))
      .mapError(mapToHttpError(_))

}
