package com.joliciel.jochre.server.api.analysis

import com.joliciel.jochre.server.api.TapirSchemaSupport
import com.joliciel.jochre.server.api.model._
import sttp.tapir.Codec.{PlainCodec, XmlCodec}
import sttp.tapir.SchemaType.{SInteger, SString}
import sttp.tapir.{Codec, CodecFormat, DecodeResult, Schema}

import java.util.UUID

trait AnalysisSchemaSupport extends TapirSchemaSupport {
  implicit val codec_projectId: Codec[String, ProjectCode, CodecFormat.TextPlain] =
    Codec.string.mapDecode(s => DecodeResult.Value(ProjectCode(s)))(_.code)
  implicit val schema_projectId: Schema[ProjectCode] = Schema(SString(), description = Some("ProjectId"))

  implicit val codec_batchId: Codec[String, BatchId, CodecFormat.TextPlain] =
    Codec.string.mapDecode(s => DecodeResult.Value(BatchId(UUID.fromString(s))))(_.id.toString)
  implicit val schema_batchId: Schema[BatchId] = Schema(SString(), description = Some("BatchId"))

  implicit val codec_fileId: Codec[String, FileId, CodecFormat.TextPlain] =
    Codec.string.mapDecode(s => DecodeResult.Value(FileId(s)))(_.id)
  implicit val schema_fileId: Schema[FileId] = Schema(SString(), description = Some("FileId"))

  implicit val codec_analysisRequestId: Codec[String, AnalysisRequestId, CodecFormat.TextPlain] = implicitly[PlainCodec[Long]].map(AnalysisRequestId(_))(_.id)
  implicit val schema_analysisRequestId: Schema[AnalysisRequestId] = schemaForLong.description("AnalysisRequestId")

  implicit val schema_imageRef: Schema[ImageReference] = Schema.derived
  implicit val schema_batchRequest: Schema[BatchRequest] = Schema.derived

  implicit val schema_analysisStatusCode: Schema[AnalysisStatusCode] = Schema.derivedEnumeration.defaultStringBased
  implicit val codec_analysisStatusCode: PlainCodec[AnalysisStatusCode] = Codec.derivedEnumeration.defaultStringBased

  implicit val schema_analysisStatus: Schema[AnalysisStatus] = Schema.oneOfUsingField[AnalysisStatus, AnalysisStatusCode](_.code, { (c: AnalysisStatusCode) => c.entryName })(
    AnalysisStatusCode.Pending -> Schema.derived[AnalysisStatus.Pending],
    AnalysisStatusCode.Underway -> Schema.derived[AnalysisStatus.Underway],
    AnalysisStatusCode.Available -> Schema.derived[AnalysisStatus.Available],
    AnalysisStatusCode.Failed -> Schema.derived[AnalysisStatus.Failed],
    AnalysisStatusCode.Retry -> Schema.derived[AnalysisStatus.Retry],
    AnalysisStatusCode.Deleted -> Schema.derived[AnalysisStatus.Deleted],
    AnalysisStatusCode.Expired -> Schema.derived[AnalysisStatus.Expired]
  )

  implicit val schema_map_analysisStatusCode_int: Schema[Map[AnalysisStatusCode,Int]] = Schema.schemaForMap(_.entryName)
  implicit val schema_batchStatistics: Schema[BatchStatistics] = Schema.derived

  implicit val schema_analysisRequest: Schema[AnalysisRequest] = Schema.derived
  implicit val schema_paginatedAnalysisRequests: Schema[PaginatedAnalysisRequests] = Schema.derived
}
