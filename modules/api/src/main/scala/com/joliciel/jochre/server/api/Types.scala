package com.joliciel.jochre.server.api

import zio.RIO

object Types {
  type Requirements = Any

  type AppTask[T] = RIO[Requirements, T]
}
