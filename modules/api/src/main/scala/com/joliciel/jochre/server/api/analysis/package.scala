package com.joliciel.jochre.server.api

import com.joliciel.jochre.server.api.authentication.RoleName

package object analysis {
  object Roles {
    val analysis = RoleName("analysis")
    val project = RoleName("project")
  }

  case class OkResponse(result: String = "OK")
}
