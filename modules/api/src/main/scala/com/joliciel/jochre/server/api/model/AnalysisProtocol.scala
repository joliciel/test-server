package com.joliciel.jochre.server.api.model

import io.circe.{Decoder, Encoder, KeyDecoder, KeyEncoder}

import java.util.UUID
import scala.util.Try

trait AnalysisProtocol {
  implicit val jsonDecoder_projectCode: Decoder[ProjectCode] = Decoder.decodeString.emapTry( str => Try(ProjectCode(str)))
  implicit val jsonEncoder_projectCode: Encoder[ProjectCode] = Encoder.encodeString.contramap[ProjectCode](_.code)

  implicit val jsonDecoder_batchId: Decoder[BatchId] = Decoder.decodeString.emapTry(str => Try(BatchId(UUID.fromString(str))))
  implicit val jsonEncoder_batchId: Encoder[BatchId] = Encoder.encodeString.contramap[BatchId](_.id.toString)

  implicit val jsonDecoder_fileId: Decoder[FileId] = Decoder.decodeString.emapTry( str => Try(FileId(str)))
  implicit val jsonEncoder_fileId: Encoder[FileId] = Encoder.encodeString.contramap[FileId](_.id)

  implicit val jsonDecoder_analysisRequestId: Decoder[AnalysisRequestId] = Decoder.decodeLong.emapTry(long => Try(AnalysisRequestId(long)))
  implicit val jsonEncoder_analysisRequestId: Encoder[AnalysisRequestId] = Encoder.encodeLong.contramap[AnalysisRequestId](_.id)

  implicit val jsonDecoder_batchDeliveryMode: Decoder[DeliveryMode] = Decoder.decodeString.emapTry(str => Try(DeliveryMode.withName(str)))
  implicit val jsonEncoder_batchDeliveryMode: Encoder[DeliveryMode] = Encoder.encodeString.contramap[DeliveryMode](_.entryName)

  implicit val jsonDecoder_analysisStatusCode: Decoder[AnalysisStatusCode] = Decoder.decodeString.emapTry(str => Try(AnalysisStatusCode.withName(str)))
  implicit val jsonEncoder_analysisStatusCode: Encoder[AnalysisStatusCode] =  Encoder.encodeString.contramap[AnalysisStatusCode](_.entryName)

  implicit val fooKeyEncoder: KeyEncoder[AnalysisStatusCode] = (code: AnalysisStatusCode) => code.entryName
  implicit val fooKeyDecoder: KeyDecoder[AnalysisStatusCode] = (key: String) => AnalysisStatusCode.fromEnum(key)
}
