package com.joliciel.jochre.server.api.analysis

import com.joliciel.jochre.server.api.HttpError._
import com.joliciel.jochre.server.api.Types.Requirements
import com.joliciel.jochre.server.api.authentication.{AuthenticationProvider, TokenAuthentication, ValidToken}
import com.joliciel.jochre.server.api.HttpError
import com.joliciel.jochre.server.api.model.AnalysisProtocol
import com.joliciel.jochre.server.api.model._
import sttp.capabilities.zio.ZioStreams
import sttp.model.StatusCode
import sttp.tapir.{AnyEndpoint, CodecFormat}
import sttp.tapir.generic.auto._
import sttp.tapir.json.circe._
import io.circe.generic.auto._
import sttp.tapir.ztapir._
import zio.stream.ZStream

import java.nio.charset.StandardCharsets
import java.time.ZonedDateTime
import java.util.UUID
import scala.concurrent.ExecutionContext

case class AnalysisApp(override val authenticationProvider: AuthenticationProvider, executionContext: ExecutionContext)
  extends AnalysisSchemaSupport
    with AnalysisLogic
    with TokenAuthentication
    with AnalysisProtocol
{
  implicit val ec: ExecutionContext = executionContext

  val postBatchEndpoint: ZPartialServerEndpoint[Requirements, String, ValidToken, (ProjectCode, BatchRequest), HttpError, BatchResponse, Any] =
    secureEndpoint(Roles.analysis)
      .errorOutVariant[HttpError](
        oneOfVariant[NotFound](StatusCode.NotFound, jsonBody[NotFound].description("Unknown project"))
      )
      .post
      .in("ocr")
      .in("project")
      .in(path[ProjectCode]("project").example(ProjectCode("UYL")))
      .in("batch")
      .in(jsonBody[BatchRequest].example(BatchRequestHelper.example))
      .out(jsonBody[BatchResponse].example(BatchResponseHelper.example))
      .description("Post a new batch of files for asynchronous analysis.")

  val postBatchHttp: ZServerEndpoint[Requirements, Any] =
    postBatchEndpoint.serverLogic[Requirements](token => input => (postBatchLogic _).tupled(input))

  val getBatchEndpoint: ZPartialServerEndpoint[Requirements, String, ValidToken, (ProjectCode, BatchId), HttpError, BatchStatistics, Any] =
    secureEndpoint(Roles.analysis)
      .errorOutVariant[HttpError](
        oneOfVariant[NotFound](StatusCode.NotFound, jsonBody[NotFound].description("Unknown project or batchId"))
      )
      .get
      .in("ocr")
      .in("project")
      .in(path[ProjectCode]("project").example(ProjectCode("UYL")))
      .in("batch")
      .in(path[BatchId]("batchId").example(BatchId(UUID.fromString("f4944b2c-bbd4-49c2-a0d7-dd188c564e0d"))))
      .out(jsonBody[BatchStatistics].example(BatchStatisticsHelper.example))
      .description("Retrieve a batch together with its statistics.")

  val getBatchHttp: ZServerEndpoint[Requirements, Any] =
    getBatchEndpoint.serverLogic[Requirements](_ => input => (getBatchLogic _).tupled(input))

  val getBatchSummaryEndpoint: ZPartialServerEndpoint[Requirements, String, ValidToken, (ProjectCode, BatchId), HttpError, ZStream[Any, Throwable, Byte], Any with ZioStreams] =
    secureEndpoint(Roles.analysis)
      .errorOutVariant[HttpError](
        oneOfVariant[NotFound](StatusCode.NotFound, jsonBody[NotFound].description("Unknown project or batchId"))
      )
      .get
      .in("ocr")
      .in("project")
      .in(path[ProjectCode]("project").example(ProjectCode("UYL")))
      .in("batch")
      .in(path[BatchId]("batchId").example(BatchId(UUID.fromString("f4944b2c-bbd4-49c2-a0d7-dd188c564e0d"))))
      .in("summary")
      .out(streamTextBody(ZioStreams)(CodecFormat.TextPlain(), Some(StandardCharsets.UTF_8)))
      .description("Retrieve a batch summary file.")

  val getBatchSummaryHttp: ZServerEndpoint[Requirements, Any with ZioStreams] =
    getBatchSummaryEndpoint.serverLogic[Requirements](_ => input => (getBatchSummaryLogic _).tupled(input))

  val getRequestsEndpoint: ZPartialServerEndpoint[Requirements, String, ValidToken, (ProjectCode, Option[BatchId], Option[ZonedDateTime], List[AnalysisStatusCode], Int, Int), HttpError, PaginatedAnalysisRequests, Any] =
    secureEndpoint(Roles.analysis)
      .errorOutVariant[HttpError](
        oneOfVariant[NotFound](StatusCode.NotFound, jsonBody[NotFound].description("Unknown project or batchId"))
      )
      .get
      .in("ocr")
      .in("project")
      .in(path[ProjectCode]("project").example(ProjectCode("UYL")))
      .in("requests")
      .in(query[Option[BatchId]]("batchId").example(Some(BatchId(UUID.fromString("f4944b2c-bbd4-49c2-a0d7-dd188c564e0d")))))
      .in(query[Option[ZonedDateTime]]("since").example(Some(ZonedDateTime.parse("2022-08-18T00:00Z"))))
      .in(query[List[AnalysisStatusCode]]("status").example(AnalysisStatusCode.values.toList))
      .in(query[Int](name="start").example(0))
      .in(query[Int](name="end").example(10))
      .out(jsonBody[PaginatedAnalysisRequests].example(PaginatedAnalysisRequestHelper.example))
      .description("Get a list of currently available files, optionally matching a given status.")

  val getRequestsHttp: ZServerEndpoint[Requirements, Any] =
    getRequestsEndpoint.serverLogic[Requirements](_ => input => (getFilesLogic _).tupled(input))

  val getAnalysisEndpoint: ZPartialServerEndpoint[Requirements, String, ValidToken, (ProjectCode, BatchId, AnalysisRequestId), HttpError, ZStream[Any, Throwable, Byte], Any with ZioStreams] =
    secureEndpoint(Roles.analysis)
      .errorOutVariant[HttpError](
        oneOfVariant[NotFound](StatusCode.NotFound, jsonBody[NotFound].description("Unknown project, batchId or file"))
      )
      .get
      .in("ocr")
      .in("project")
      .in(path[ProjectCode]("project").example(ProjectCode("UYL")))
      .in("batch")
      .in(path[BatchId]("batchId").example(BatchId(UUID.fromString("f4944b2c-bbd4-49c2-a0d7-dd188c564e0d"))))
      .in("request")
      .in(path[AnalysisRequestId]("analysisRequestId").example(AnalysisRequestId(42)))
      .out(streamTextBody(ZioStreams)(CodecFormat.Xml(), Some(StandardCharsets.UTF_8)))
      .description("Return the ALTO 4 XML analysis of a given file.")

  val getAnalysisHttp: ZServerEndpoint[Requirements, Any with ZioStreams] =
    getAnalysisEndpoint.serverLogic[Requirements](_ => input => (getAnalysisLogic _).tupled(input))

  val deleteAnalysisEndpoint: ZPartialServerEndpoint[Requirements, String, ValidToken, (ProjectCode, BatchId, FileId), HttpError, OkResponse, Any] =
    secureEndpoint(Roles.analysis)
      .errorOutVariant[HttpError](
        oneOfVariant[NotFound](StatusCode.NotFound, jsonBody[NotFound].description("Unknown project or fileId"))
      )
      .delete
      .in("ocr")
      .in(path[ProjectCode]("project").example(ProjectCode("UYL")))
      .in("batch")
      .in(path[BatchId]("batchId").example(BatchId(UUID.fromString("f4944b2c-bbd4-49c2-a0d7-dd188c564e0d"))))
      .in("request")
      .in(path[FileId]("file").example(FileId("ABC1234-0123")))
      .out(jsonBody[OkResponse])
      .description("Delete the ALTO 4 XML analysis for a given file after it has been retrieved.")

  val deleteAnalysisHttp: ZServerEndpoint[Requirements, Any] =
    deleteAnalysisEndpoint.serverLogic[Requirements](_ => input => (deleteAnalysisLogic _).tupled(input))

  val endpoints: List[AnyEndpoint] = List(
    postBatchEndpoint,
    getBatchEndpoint,
    getBatchSummaryEndpoint,
    getRequestsEndpoint,
    getAnalysisEndpoint,
    deleteAnalysisEndpoint,
  ).map(_.endpoint)

  val http: List[ZServerEndpoint[Requirements, Any with ZioStreams]] = List(
    postBatchHttp,
    getBatchHttp,
    getBatchSummaryHttp,
    getRequestsHttp,
    getAnalysisHttp,
    deleteAnalysisHttp
  )
}
