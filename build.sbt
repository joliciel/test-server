import BuildHelper._
import Libraries._
import com.typesafe.sbt.packager.docker.Cmd

lazy val scala2Version = "2.13.13"
lazy val scala3LTSVersion = "3.3.3"
lazy val scala3Version = "3.4.0"
lazy val supportedScalaVersions = List(scala2Version, scala3LTSVersion, scala3Version)

ThisBuild / scalaVersion := scala2Version
ThisBuild / organization := "com.joli-ciel"
ThisBuild / homepage     := Some(url("https://www.joli-ciel.com/"))
ThisBuild / licenses     := List("AGPL-v3" -> url("https://www.gnu.org/licenses/agpl-3.0.en.html"))

val cloakroomVersion = "0.5.15"

lazy val jochre3ServerVersion = sys.env.get("JOCHRE3_SERVER_VERSION")
  .getOrElse{
    ConsoleLogger().warn("JOCHRE3_SERVER_VERSION env var not found")
    "0.0.1-SNAPSHOT"
  }

val projectSettings = commonSettings ++ Seq(
  version := jochre3ServerVersion,
)

testFrameworks += new TestFramework("zio.test.sbt.ZTestFramework")

lazy val root =
  Project(id = "jochre3-server", base = file("."))
    .settings(noDoc: _*)
    .settings(noPublishSettings: _*)
    .settings(
      crossScalaVersions := Nil,
    )
    .aggregate(api)

lazy val api = project
  .in(file("modules/api"))
  .enablePlugins(JavaServerAppPackaging, DockerPlugin)
  .settings(projectSettings: _*)
  .settings(
    crossScalaVersions := supportedScalaVersions,
    libraryDependencies ++= commonDeps ++ databaseDeps ++ apiDeps ++ Seq(
      "com.safety-data" %% "cloakroom-scala" % cloakroomVersion,
      "com.safety-data" %% "cloakroom-test-util-scala" % cloakroomVersion % Test,
      "com.safety-data" % "cloakroom" % cloakroomVersion,
      "com.github.cb372" %% "cats-retry" % "3.1.0"
    ),
    Docker / packageName := "jochre/jochre3-server",
    Docker / maintainer  := "Joliciel Informatique SARL",
    Docker / daemonUserUid := Some("1001"),
    dockerBaseImage := "openjdk:17.0.2-bullseye",
    Docker / dockerRepository := sys.env.get("JOCHRE3_DOCKER_REGISTRY"),
    Docker / version     := version.value,
    dockerExposedPorts := Seq(3232),
    dockerExposedVolumes := Seq("/opt/docker/index"),
    // Add docker commands before changing user
    Docker / dockerCommands := dockerCommands.value.flatMap {
      case Cmd("USER", args@_*) if args.contains("1001:0") => Seq(
        // Add unattended security upgrades to docker image
        Cmd("RUN", "apt update && apt install -y unattended-upgrades"),
        Cmd("RUN", "unattended-upgrade -d --dry-run"),
        Cmd("RUN", "unattended-upgrade -d"),
        Cmd("USER", args: _*)
      )
      case cmd => Seq(cmd)
    },
    //do not package scaladoc
    Compile/ packageDoc / mappings := Seq(),
    Compile / mainClass := Some("com.joliciel.jochre.server.api.MainApp"),
    fork := true,
  )
