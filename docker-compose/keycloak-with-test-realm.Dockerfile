FROM jboss/keycloak:15.1.0
COPY ./realm.json /realm.json

CMD ["-Dkeycloak.profile.feature.admin_fine_grained_authz=enabled"]
