FROM flyway/flyway:9.8.1
ADD ./ /flyway/sql


ENTRYPOINT ["/bin/sh", "-c"]
CMD ["/flyway/flyway -url=jdbc:postgresql://${POSTGRES_HOST?}:${POSTGRES_PORT?}/${POSTGRES_DATABASE?} -user=${POSTGRES_USER?} -password=${POSTGRES_PASSWORD?} migrate"]